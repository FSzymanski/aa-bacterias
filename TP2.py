import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Button
from sklearn.cluster import DBSCAN
from sklearn.cluster import AffinityPropagation
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif, chi2
from sklearn.manifold import Isomap
from sklearn.manifold import TSNE
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn import metrics
from matplotlib.colors import ListedColormap
from sklearn import neighbors
import tp2_aux as helper
from sklearn.metrics import adjusted_rand_score, silhouette_score


def loadLabels(file_name):
    rows = []
    lines = open(file_name).readlines()
    for line in lines[:]:
        parts = line.split(',')
        rows.append((float(parts[0]), float(parts[1])))
    return np.array(rows)


def pca(images):
    pca = PCA(n_components=6)
    features = pca.fit_transform(images)
    return features


def tSNE(images):
    model = TSNE(method='exact', n_components=6, random_state=0)
    features = model.fit_transform(images)
    return features


def isomap(images):
    iso = Isomap(n_components=6)
    features = iso.fit_transform(images)
    return features


def visualizeFeatures(features, labels):
    fvalue_selector = SelectKBest(f_classif, k='all')
    k_best_features = fvalue_selector.fit_transform(features, labels[:, 1])
    plt.plot(range(0, 18), fvalue_selector.scores_, '.r');
    plt.show()


def features_selection(features, labels, _k):
    print("featureSeleciton");
    visualizeFeatures(features, labels)

    fvalue_selector = SelectKBest(f_classif, k=_k)
    k_best_features = fvalue_selector.fit_transform(features, labels[:, 1])
    best_std = StandardScaler().fit_transform(k_best_features)
    return best_std


def get_distances(features, labels):
    kth_dist = []
    neigh = NearestNeighbors(n_neighbors=5).fit(features)
    distances = neigh.kneighbors(features, return_distance=True)

    for i in range(0, len(distances[0])):
        kth_dist.append(distances[0][i][4])

    kth_dist.sort()
    kth_dist = kth_dist[::-1]
    zero_diff = []

    for i in range(1, len(kth_dist)):
        diff = kth_dist[i - 1] - kth_dist[i]
        if diff == 0:
            zero_diff.append(i)

    return kth_dist, zero_diff


class DBSCAN_selector(object):
    index = 0
    features = []
    true_labels = []
    kth_dist = []
    zero_changes = []
    rand_score = 0
    sil_score = 0
    fig, (dist_plt, dbscan_plt) = plt.subplots(1, 2, figsize=(10, 5))
    chosen_dist = 100000
    db = None

    def __init__(self, Features, labels):
        self.features = Features
        self.true_labels = labels
        self.kth_dist, self.zero_changes = get_distances(self.features, self.true_labels)
        self.dbscanDraw()

    def next(self, event):
        if self.index < len(self.kth_dist) - 1:
            self.index += 1
            self.dbscanDraw()

    def prev(self, event):
        if (self.index != 0):
            self.index -= 1
        self.dbscanDraw()

    def accept(self, event):
        self.chosen_dist = self.kth_dist[self.zero_changes[self.index]]
        plt.savefig("dbscan_parameters_selector.png")
        plt.close()

    def dbscanDraw(self):
        plt.close(self.fig)
        self.fig, (self.dist_plt, self.dbscan_plt) = plt.subplots(1, 2, figsize=(10, 5))
        self.dist_plt.plot(range(0, len(self.kth_dist)), self.kth_dist)
        self.dist_plt.plot(self.zero_changes[self.index], self.kth_dist[self.zero_changes[self.index]], 'ro')

        self.db = DBSCAN(eps=self.kth_dist[self.zero_changes[self.index]], min_samples=5).fit(self.features)
        core_samples_mask = np.zeros_like(self.db.labels_, dtype=bool)
        core_samples_mask[self.db.core_sample_indices_] = True
        labels = self.db.labels_
        self.rand_score = adjusted_rand_score(self.true_labels[:, 1], labels)
       # self.sil_score = silhouette_score(self.true_labels[:, 1], labels)


        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        unique_labels = set(labels)
        colors = [plt.cm.Spectral(each)
                  for each in np.linspace(0, 1, len(unique_labels))]
        for k, col in zip(unique_labels, colors):
            if k == -1:
                # Black used for noise.
                col = [0, 0, 0, 1]

            class_member_mask = (labels == k)

            xy = self.features[class_member_mask & core_samples_mask]
            self.dbscan_plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                                 markeredgecolor='k', markersize=14)

            xy = self.features[class_member_mask & ~core_samples_mask]
            self.dbscan_plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                                 markeredgecolor='k', markersize=6)

        self.fig.suptitle('Eps: %f, Clusters: %d, Rand_score: %f' % (self.kth_dist[self.zero_changes[self.index]], n_clusters_, self.rand_score))
        axprev = plt.axes([0.9, 0.40, 0.1, 0.075])
        axnext = plt.axes([0.9, 0.25, 0.1, 0.075])
        axaccept = plt.axes([0.9, 0.10, 0.1, 0.075])
        bnext = Button(axprev, 'Next')
        bnext.on_clicked(self.next)
        bprev = Button(axnext, 'Previous')
        bprev.on_clicked(self.prev)
        baccept = Button(axaccept, 'Accept')
        baccept.on_clicked(self.accept)
        plt.show()


def affinity(features, true_labels, preference):
    db = AffinityPropagation(preference=preference).fit(features)
    silhouette = silhouette_score(features, db.labels_)

    return adjusted_rand_score(true_labels[:, 1], db.labels_), silhouette_score(features, db.labels_)


def affinity_all(features, true_labels):
    #dbscan_model = DBSCAN_selector(features, true_labels)  #Visual selection of eps. Method described in the paper, dont touch
    #helper.report_clusters(true_labels[:, 0], dbscan_model.db.labels_, "manual_eps.html")

    parameters_to_check = np.arange(-100, 0, 2)

    parameters = []
    resultsAdjusted = []
    resultsSilhouette = []

    for i in parameters_to_check:
        resultsAd, resultSilhouette = affinity(features, true_labels, i)
        resultsAdjusted.append(resultsAd)
        parameters.append(i)

    best_index = resultsAdjusted.index(min(resultsAdjusted))
    print(best_index)
    print(min(resultsAdjusted))
    # print("kmeans best:\n   eps: %f\n   score: %f" %(parameters[best_index], results[best_index]))
    # helper.report_clusters(true_labels[:, 0], DBSCAN(eps=parameters[best_index], min_samples=5).fit(features).labels_,
    #                        " auto_eps:%f_score:%f_dbscan.html" %(parameters[best_index], results[best_index]))
    helper.report_clusters(true_labels[:, 0], AffinityPropagation(preference=parameters[best_index]).fit(features).labels_,
                           "affinity.html")


def dbscan(features, true_labels, eps):
    db = DBSCAN(eps=eps, min_samples=5).fit(features)

    silhouette = 0
    n_clusters_ = len(set(db.labels_)) - (1 if -1 in true_labels else 0)

    if(n_clusters_ > 1):
        silhouette = silhouette_score(features, db.labels_)

    return adjusted_rand_score(true_labels[:, 1], db.labels_), silhouette

def dbscan_all(features, true_labels):
    dbscan_model = DBSCAN_selector(features, true_labels)  #Visual selection of eps. Method described in the paper, dont touch
    helper.report_clusters(true_labels[:, 0], dbscan_model.db.labels_, "manual_eps.html")

    parameters_to_check = np.arange(0.0, 2.0, 0.05)

    parameters = []
    resultsAdjusted = []
    resultsSilhouette = []

    for i in parameters_to_check:
        if i > 0:
            resultAd, resultSilhouette = dbscan(features, true_labels, i)
            resultsAdjusted.append(resultAd)
            parameters.append(i)

    plt.plot(parameters, resultsAdjusted, '.r');
    plt.savefig("dbcan_parameters_vs_randScore");

    best_index = resultsAdjusted.index(min(resultsAdjusted))
    # print("kmeans best:\n   eps: %f\n   score: %f" %(parameters[best_index], results[best_index]))
    # helper.report_clusters(true_labels[:, 0], DBSCAN(eps=parameters[best_index], min_samples=5).fit(features).labels_,
    #                        " auto_eps:%f_score:%f_dbscan.html" %(parameters[best_index], results[best_index]))
    helper.report_clusters(true_labels[:, 0], DBSCAN(eps=parameters[best_index], min_samples=5).fit(features).labels_,
                           "dbscan_best_automatic.html")


def kmeans_single(features, true_labels, k):
    kmeans = KMeans(n_clusters=k, random_state=0).fit(features)

    silhouette = 0
    n_clusters_ = len(set(kmeans.labels_)) - (1 if -1 in true_labels else 0)

    if(n_clusters_ > 1):
        silhouette = silhouette_score(features, kmeans.labels_)
    return adjusted_rand_score(true_labels[:, 1], kmeans.labels_), silhouette


def kmeans_all(features, true_labels):
    parameters_to_check = np.arange(1, 20, 1)

    parameters = []
    resultsAdjusted = []

    for i in parameters_to_check:
        if i > 0:
            resultAdjusted, resultSilhouette = kmeans_single(features, true_labels, i)
            print(resultSilhouette)
            resultsAdjusted.append(resultAdjusted)
            parameters.append(i)

    best_index = resultsAdjusted.index(max(resultsAdjusted))
    print("kmeans best:\n   k: %f\n   score: %f" % (parameters[best_index], resultsAdjusted[best_index]))
    # helper.report_clusters(true_labels[:, 0], KMeans(n_clusters=parameters[best_index], random_state=0).fit(features).labels_,
    #                        " auto_k:%f_score:%f_kmean.html" % (parameters[best_index], results[best_index]))
    helper.report_clusters(true_labels[:, 0], KMeans(n_clusters=parameters[best_index], random_state=0).fit(features).labels_,
                           "kmeans_best_automatic.html")
    return parameters[best_index], resultsAdjusted[best_index]


def get_features():
    features_number = 6
    images = helper.images_as_matrix()
    labels = loadLabels("labels.txt")

    if os.path.isfile("features.npz"):
        npzfile = np.load("features.npz")
        best_features = npzfile['features']
        return best_features, labels

    else:
        pca_features = pca(images)
        tSNE_features = tSNE(images)
        isomap_features = isomap(images)

        all_features = np.column_stack((pca_features, tSNE_features, isomap_features))
        best_features = features_selection(all_features, labels, features_number)

        np.savez("features.npz", features=best_features)
        return best_features, labels


def main():
    best_features, labels = get_features()
    dbscan_all(best_features, labels)
    kmeans_all(best_features, labels)
    affinity_all(best_features, labels)


if __name__ == "__main__":
    main()
